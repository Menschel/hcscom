Tutorial
========

The basic usage of this module always requires importing the module
and instantiating HcsCom with a valid serial port.

code::

    from hcscom import HcsCom
    port = r"/dev/serial/by-id/usb-Silicon_Labs_CP2102_USB_to_UART_Bridge_Controller_0001-if00-port0"
    # this is a common port on linux systems
    hcs = HcsCom(port=port)

If everything went right, we can print basic informations about the device we found.

code::

    print(hcs)

We can set the voltage to 12V.

code::

    hcs.set_voltage(12)

We can read the display content.

code::

    print(hcs.get_display_status())
