.. hcscom documentation master file, created by
   sphinx-quickstart on Wed Mar 17 14:05:15 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to hcscom's documentation!
==================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   usage/api
   usage/tutorial



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
